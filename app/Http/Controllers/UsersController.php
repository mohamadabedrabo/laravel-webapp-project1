<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use App\Department;
use App\Role;
use App\Userrole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = Candidate::all();
        $users = User::all();
        $statuses = Status::all();  
        $roles = Role::all();      
        return view('users.index', compact('candidates','users', 'statuses','roles'));        
    }

    public function details($id)
    {        
        //$users = User::findOrFail($id);  
        $users = User::all();  
        $candidates = Candidate::all(); 
        $departments = Department::all();  
        return view('users.details', compact('users','departments','candidates'));
    }

    public function makemanager($uid){
        Gate::authorize('assign-user');
        $user = User::findOrFail($uid);
        $userrole = new Userrole();
        $userrole->role_id = 2;
        $userrole->user_id = $uid;
        $userrole->save(); 
        return redirect()->back()->with('message', 'Permission changed succesfully!!');
        //return back();
        //return redirect('users');
    }

    public function deletemanager($user_id){
        //Gate::authorize('assign-user');
        $userrole = Userrole::findOrFail($user_id);
        $userrole->delete();
        return redirect()->back()->with('message', 'deleted!!');

    }

    public function changedepartment($uid, $did = null){
        Gate::authorize('assign-user');
        $user = User::findOrFail($uid);
        $user->department_id = $did;
        $user->save(); 
        return back();
    }

    

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('assign-user');
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users'); 
    }
}
