@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Role</th><th>Details</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>   
            <td>
                @foreach($user->roles as $role)
                    {{$role->name}} 
                @endforeach
            </td>
            <td>
                <a href="{{ route('user.details',$user->id)}}">Details</a>
            </td>                             
            <td>
                <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td>   
            <td>
                <a href = "{{route('user.makemanager',$user->id)}}">make manager</a>
            </td>    
            <td>
            @foreach($user->roles as $role)
                @if (isset($user->roles))
                    <a href = "{{route('user.deletemanager',$user->id)}}">delete manager</a>
                @else
                    not manager
                @endif
            @endforeach
            </td>                                                              
        </tr>
    @endforeach
</table>
@endsection
