@extends('layouts.app')

@section('title', 'Users')

@section('content')
<h1>details of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>  
            <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  
                        @if (isset($user->department_id))
                           {{$user->department->name}}
                        @else
                            Assign department
                        @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach($departments as $department)
                            @if (App\User::responsible($user->id) == null )
                                <a class="dropdown-item" href="{{route('user.changedepartment',[$user->id,$department->id])}}">{{$department->name}}</a>
                            @else
                        <p class="alert alert-info">{{ 'notallowed You are not allowed to change the department because user have candidate\s' }}</p>
                            @endif
                        @endforeach
                    </div>
                </div>                
            </td>                                                          
        </tr>
    @endforeach
</table>
@endsection

